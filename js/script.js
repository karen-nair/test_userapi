(function ($) {

    var userID = null;

    var client = {

        fireAjax: function (arguments, callback) {

            $.ajax(
                {
                    url: frontend.ajax_url,
                    'data': arguments,
                    error: function (msg, b, c) {

                        console.debug('error');
                        console.debug(msg);
                        console.debug(b);
                        console.debug(c);
                    },
                    dataType: 'json',
                    cache: false,
                    success: function (result) {
                        callback(result);
                        return false;
                    },
                    type: 'POST'
                }
            );

        },

        renderUserPosts: function (posts) {

            var template = '<div class="large-4 medium-12 small-12 cell"><table data-table-posts><tr><th colspan="2">Posts</th></tr>';
      
            var counter = 0;
            for (var i=0; i < posts.length; i++) {
                 counter++;
                 template +='<tr data-post id="'+posts[i].id+'"><td>'+counter+'</td><td><a href="javascript:void(0)">'+posts[i].title+'</a></td></tr>';
            }

            template += '</table></div>';
            return template;
        },


        renderUserTodos: function (todos) {

            var template = '<div class="large-4  medium-12 small-12 cell"><table data-todos><tr><th colspan="2">Todos</th><th>Status</th></tr>';
            var status = 'pending';
            var counter = 0;
            for (var i=0; i < todos.length; i++) {
                 counter++;
                if (todos[i].completed == true) {
                    status= 'done';
                }

                template +='<tr id="'+todos[i].id+'"><td>'+counter+'</td><td>'+todos[i].title+'</td><td>'+
                status
                +'</td></tr>';
            }

            template += '</table></div>';
            return template;
        },


        renderUserAlbums: function (todos) {

            var template = '<div class="large-4  medium-4 cell"><table><tr><th colspan="2">Albums</th></tr>';
      
            var counter = 0;

            for (var i=0; i < todos.length; i++) {
                 counter++;

                 template +='<tr data-album id="'+todos[i].id+'"><td>'+counter+'</td><td><a href="javascript:void(0)">'+todos[i].title+'</a></td></tr>';
            }

            template += '</table></div>';
            return template;
        },

        renderAlbumPhotos: function (todos) {

            var template = '<div class="grid-x grid-padding-x">';
      
            var counter = 0;
            for (var i=0; i < todos.length; i++) {
                 counter++;
                  template += '<div class="large-4 medium-4 small-4 cell" id="'+todos[i].id+'">'+
                     '<div class="primary callout">'+
                      '<p>'+counter+' '+todos[i].title+'</p>'+
                 '<img src="'+todos[i].thumbnailUrl+'">'+
                     '</div>'+
                   '</div>';
            }

            template += '</div>';
            return template;
        },

        renderUserPostsComments: function (posts) {

            var template = '<table data-table-comments><tr><th colspan="3">Comments</th></tr><tr><th>Name</th><th>Email</th><th>Body</th></tr>';
      

            for (var i=0; i < posts.length; i++) {

                 template +='<tr id="'+posts[i].id+'">'+
                 '<td>'+posts[i].name+'</td>'+
                 '<td>'+posts[i].email+'</td>'+
                 '<td>'+posts[i].body+'</td>'+
                 '</tr>';
            }

            template += '</table>';
            return template;
        }

    };

    /*user mandatory links clicked*/
    $('#userTable tr td a').on(
        'click', function (e) {

            e.preventDefault();
            var currentItem = $(this);
            userID = currentItem.parents('tr').attr('id');
            $('#userTable').find('tr[data-user-detail-wrapper]').remove();
            $('#userTable').find('tr').not(currentItem).css('background', 'none');

            currentItem.parents('tr').css('background-color', 'lightgrey');

            currentItem.parents('tr').after('<tr data-user-detail-wrapper><td colspan="8"><div class="grid-x grid-padding-x" data-user-detail>');

            var result1 = null;
            var result2 = null;
            var result3 = null;
            var template = null;

            //multiple request at a time
            $.when(
                client.fireAjax(
                    {
                        'action':'getUserPosts',
                        'data':{'userID':userID}
                    }, function (result) {
            
                        if (typeof result == 'object') {
                            result1 =result;
                        }  else {
                            result1 = JSON.parse(result); 
                        }
            
                        template = client.renderUserPosts(result1);  


                        currentItem.parents('tr').next('tr').find('div[data-user-detail]').append(template);

                    }
                ),
                client.fireAjax(
                    {
                        'action':'getUserTodos',
                        'data':{'userID':userID}
                    }, function (result) {
                        if (typeof result == 'object') {
                            result2 =result;
                        }  else {
                            result2 = JSON.parse(result); 
                        }

                        template = client.renderUserTodos(result2);;    

                        currentItem.parents('tr').next('tr').find('div[data-user-detail]').append(template);



                    }
                ),
                client.fireAjax(
                    {
                        'action':'getUserAlbums',
                        'data':{'userID':userID}
                    }, function (result) {
            
                        if (typeof result == 'object') {
                            result3 =result;
                        }  else {
                            result3 = JSON.parse(result); 
                        }

                        template = client.renderUserAlbums(result3);;    
        
                        currentItem.parents('tr').next('tr').find('div[data-user-detail]').append(template);



                    }
                )
            ).done(function () {});
        


        
        
        
        
        }
    );

    /*post clicked*/
    $('#userTable').on(
        'click', 'tr[data-post] td a', function () {

            $(this).parents('tr[data-post]').css('background-color', 'lightgrey');
            $('#userTable').find('tr[data-post-detail]').remove();

            var postID = $(this).parents('tr').attr('id');
            var currentItem = $(this).parents('tr[data-post]');
            var comments = null;
        

        

            client.fireAjax(
                {
                    'action':'getPostComments',
                    'data':{'postID':postID}
                }, function (result) {

                    if (typeof result == 'object') {
                        comments =result;
                    }  else {
                        comments = JSON.parse(result); 
                    }

                    var a = client.renderUserPostsComments(comments);
                    currentItem.after('<tr data-post-detail><td colspan="2">'+a+'</td></tr>');

                }
            );

        }
    );

    //album clicked
    $('#userTable').on(
        'click', 'tr[data-album] td a', function () {

            $('#userTable').find('tr[data-album-detail]').remove();

            $(this).parents('tr[data-album]').css('background-color', 'lightgrey');
            var albumID = $(this).parents('tr').attr('id');
            var currentItem = $(this).parents('tr[data-album]');
            var photos = null;
        

            client.fireAjax(
                {
                    'action':'getAlbumPhotos',
                    'data':{'albumID': albumID}
                }, function (result) {
                    if (typeof result == 'object') {
                        photos =result;
                    }  else {
                        photos = JSON.parse(result); 
                    }

                    var a = client.renderAlbumPhotos(photos);
                    currentItem.after('<tr data-album-detail><td colspan="8">'+a+'</td></tr>');
                    return false;
                }
            );

        }
    );

})(jQuery);





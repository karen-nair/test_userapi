## Installation

Requirements for the plugin installation:

1. It's assumed that you have wordpress installed in your machine prior to installing this plugin.
2. It's also assumed that composer is installed in your machine.

**Composer allows easy download of dependancies. For instance, in order to download this plugin, follow the steps below.**

3. Navigate to your project root folder.
4. Create **composer.json** file in the root folder.The directory structure should look like:
    /my_wordpress_project/composer.json
            
5. Add the following content into your composer.json file:

```javascript
{
    "require-dev": {
        "usr/userapi": "dev-master"
    },
    "repositories":[
    {
        "type": "vcs",
        "url" : "https://{your-bitbucket-username}@bitbucket.org/karen-nair/test_userapi.git"
    }
    ]
}
```


7. After successful installation, navigate to my_wordpress_project/wp-content/plugins and you should see plugin named **userapi**

8. Login to wordpress admin panel and activate the plugin.
9. Remember to visit setting->permalink and click save. This will register our plugin's new endpoint.
10. Now navigate to www.myproject.com/userapi and you should see table populated with user data fetched from external API provided.


## Run phpunit test

Navigate to userapi plugin root and run the below script:

```javascript
composer run-script test
```
On successful test, you should see:

*OK (2 tests, 15 assertions)*

test file is located in *\wp-content\plugins\userapi\tests\phpunit\testcases*
There're two menthods created to test ajax hooks their functions.

## HTTP Cache

User data is fetched on page load when the endpoint **userapi** is visited. However it only fetches once form the server. Assuming this is static response which will not change,
subsequesnt requests are read from cache files created by an internal function.
All the files created have .json.cache extension and they're located in:

*\wp-admin*

sample file name:
*_getAlbumPhotos_1_.json.cache*

## Responive front end

Zurb foundation framework is used in making the plugin template responsive.

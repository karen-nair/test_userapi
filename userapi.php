<?php
/**
 * Plugin Name: User Api
 * Author: Kalaivani Nair @ Karen
 * Description: Inpsyde assignemnt
 * Version: 1.0
 * php version 7.2.1
 *
 * @category Helper_File
 * @package  User_Api
 * @author   Kalaivani Nair <kalaivani.dc@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @version  GIT: git@github.com:karen-nair/testcomposer2.git
 * @link     mailto:kalaivani.dc@gmail.com 
 */

//constants
define('PLUGIN_KEYWORD', 'USR');
//load framework
require 'bootstrap.php';

//initialize plugin
add_action('plugins_loaded', 'loadPlugin');

/**
 * Initiates plugin
 *
 * Start instantiating core file and registeres relevant hooks
 *
 * @return object USR\USR_Core instance
 */
function loadPlugin()
{
    /*autoload*/
    spl_autoload_register('autoloadFiles');
    $core = new USR\USR_Core;
    $core->addHooks();
}




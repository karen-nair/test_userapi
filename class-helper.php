<?php
/**
 * USR_Helper
 *
 * Class file handles helper methods for the plugin to operate
 *
 * php version 7.2.1
 *
 * @category Helper_File
 * @package  User_Api
 * @author   Kalaivani Nair <kalaivani.dc@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @version  GIT: git@github.com:karen-nair/testcomposer2.git
 * @link     mailto:kalaivani.dc@gmail.com 
 */
namespace USR;
/**
 * Offers helper functions
 *
 * Offers helper functions for core class
 *
 * @category Core_File
 * @package  User_Api
 * @author   Kalaivani Nair <kalaivani.dc@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     mailto:kalaivani.dc@gmail.com  
 */
class USR_Helper
{
    /**
     * Construct function
     */
    public function __construct()
    {    
        
    }

    /**
     * Custom define CURL function
     *
     * Checks if results exists in cache before firing CURL request
     *
     * @param array $request  string handle to unique identify cache file
     * @param $url      string api url to fetch
     * @param $noEncode tells if result should be json_encoded or not
     *
     * @return null
     * @since  1.0
     */
    public static function fireCURL($request, $url, $noEncode = false)
    {


        $cacheFileName = $request.'.json.cache.txt';
        $cacheUpdateMin = 30;

        $results = array();
        // Check if the file exists and file timestamp against current time.
        if (!file_exists($cacheFileName) 
            || filemtime($cacheFileName) + ($cacheUpdateMin * 60)            < time()
        ) {


            // Curl request to get the json.
            $opts = array(
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 60,
            CURLOPT_USERAGENT => 'get-request',
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_FILETIME => true,
            // CURLOPT_HEADER => true,
            CURLOPT_HTTPHEADER => array('Accept: application/json'),
            CURLOPT_URL => $url
            );

            $ch = curl_init();

            curl_setopt_array($ch, $opts);


            $results = curl_exec($ch);

            // Write to the cache file.
            file_put_contents($cacheFileName, $results);

        } else {

            $results = json_decode(file_get_contents($cacheFileName));
            
            return $results;

        }
 




        if ($noEncode) {

            return $results;
        }
        return json_decode($results, true);
    }

    /**
     * Custom define POST CURL function
     *
     * Checks if results exists in cache before firing CURL request
     *
     * @param $url      string api url to fetch
     * @param array $data     input for CURL requests
     * @param $noEncode tells if result should be json_encoded or not
     *
     * @return null
     * @since  1.0
     */
    public static function fireCURLPOST($url, $data, $noEncode = false)
    {

        $opts = array(
        CURLOPT_CONNECTTIMEOUT => 10,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_TIMEOUT => 60,
        CURLOPT_USERAGENT => 'post-rquest',
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_HTTPHEADER => array('Accept: application/json'),
        CURLOPT_POSTFIELDS => http_build_query($data),
        CURLOPT_POST => 1,
        CURLOPT_URL => $url
        );

        $ch = curl_init();

        curl_setopt_array($ch, $opts);

        $result = curl_exec($ch);



        if ($noEncode) {

            return $result;
        }

        return json_decode($result, true);
    }
    
    /**
     * Filter request
     *
     * Filter request before passing to fireCURL 
     *
     * @param array $request  string handle to unique identify cache file
     * @param $url      string api url to fetch
     * @param $noEncode tells if result should be json_encoded or not
     *
     * @return array result if true
     * @since  1.0
     */
    public static function handleRequest($request, $url, $noEncode = false)
    {
        try {

            $result = USR_Helper::fireCURL($request, $url, $noEncode);

            if (!$result) {
                echo "error in fetching api";exit;
            }
            return json_encode($result);
        }
        catch(Exception $e) {
            echo 'Message: ' .$e->getMessage();
        }
    }

     /**
      * Adapter for getUsers
      *
      * Adapter fr getUsers function
      *
      * @param array $data parameters for requests
      *
      * @return array consist of url, request
      * @since  1.0
      */
    private static function _getUsersAdapter($data)
    {
        return array(
        'url'=>'https://jsonplaceholder.typicode.com/users',
        'request'=>'_request_'
        );
    }

     /**
      * Adapter for getUserPosts
      *
      * Adapter fr getUserPosts function
      *
      * @param array $data parameters for requests
      *
      * @return array consist of url, request
      * @since  1.0
      */
    private static function _getUserPostsAdapter($data)
    {
        extract($data);
        if (!$userID) {
            return;
        }
        return array(
                    'url'=>
                    'https://jsonplaceholder.typicode.com/posts?userId='
                    .$userID,
                    'request'=>'_request_'.$userID.'_'
        );
    }

    /**
     * Adapter for getUserTodos
     *
     * Adapter fr getUserTodos function
     *
     * @param array $data parameters for requests
     *
     * @return array consist of url, request
     * @since  1.0
     */
    private static function _getUserTodosAdapter($data)
    {
        extract($data);
        if (!$userID) {
            return;
        }
        return array(
                    'url'=>
                    'https://jsonplaceholder.typicode.com/users/'
                    .$userID.'/todos',
                    'request'=>'_request_'.$userID.'_'
        );
    }

    /**
     * Adapter for getUserAlbums
     *
     * Adapter fr getUserAlbums function
     *
     * @param array $data parameters for requests
     *
     * @return array consist of url, request
     * @since  1.0
     */
    private static function _getUserAlbumsAdapter($data)
    {
        extract($data);
        if (!$userID) {
            return;
        }
        return array(
                    'url'=>
                    'https://jsonplaceholder.typicode.com/users/'
                    .$userID.'/albums',
                    'request'=>'_request_'.$userID.'_'
        );
    }

    /**
     * Adapter for getAlbumPhotos
     *
     * Adapter fr getAlbumPhotos function
     *
     * @param array $data parameters for requests
     *
     * @return array consist of url, request
     * @since  1.0
     */
    private static function _getAlbumPhotosAdapter($data)
    {
        extract($data);
        if (!$albumID) {
            return;
        }
        return array(
                    'url'=>
                    'https://jsonplaceholder.typicode.com/albums/'
                    .$albumID.'/photos',
                    'request'=>'_request_'.$albumID.'_'
        );
    }
    /**
     * Adapter for getPostComments
     *
     * Adapter fr getPostComments function
     *
     * @param array $data parameters for requests
     *
     * @return array consist of url, request
     * @since  1.0
     */
    private static function _getPostCommentsAdapter($data)
    {
        extract($data);
        if (!$postID) {
            return;
        }
        return array(
                    'url'=>
                    'https://jsonplaceholder.typicode.com/posts/'
                    .$postID.'/comments',
                    'request'=>'_request_'.$postID.'_'
        );
    }
    
    /**
     * Design request handle
     *
     * Design request handle to be used for creating cache file
     *
     * @param string $action  request name
     * @param array  $adapter adapter filtered array
     *
     * @return array changed adapter element
     * @since  1.0
     */
    private static function _designRequest($action, $adapter)
    {
        $adapter['request'] = str_replace(
            "_request_", '_'.$action.'_', 
            $adapter['request']
        );
        return $adapter;
    }

    /**
     * Filters api
     *
     * Filter api arguments for CURL request
     *
     * @param string $action handle for request                      
     * @param array  $data   iput for CURL request
     *
     * @return array adapter for CURL request
     *
     * @since 1.0
     */
    public static function apiAdapter($action, $data = false)
    {
        return self::_designRequest($action, self::{'_'.$action.'Adapter'}($data));
    }
}

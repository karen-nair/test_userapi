<?php
/**
 * USR_Core
 *
 * Class file handles core methods for the plugin to operate
 *
 * php version 7.2.1
 *
 * @category Core_File
 * @package  User_Api
 * @author   Kalaivani Nair <kalaivani.dc@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @version  GIT: git@github.com:karen-nair/testcomposer2.git
 * @link     mailto:kalaivani.dc@gmail.com 
 */
namespace USR;

use USR\USR_Helper as helper;
/**
 * Registers core functions
 *
 * Registers core functions of plugin since plugin_loaded called
 *
 * @category Core_File
 * @package  User_Api
 * @author   Kalaivani Nair <kalaivani.dc@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     mailto:kalaivani.dc@gmail.com  
 */
class USR_Core
{
    private $_postTypes = null;
    private $_capability = null;
    private $_parentMenuSlug = null;

    /**
     * Defines default variables
     *
     * Assigns value to default variables
     */
    public function __construct()
    {    

        $this->_postTypes = array(
        'userapi'
        );

        $this->_capability = 'manage_options';
        $this->_parentMenuSlug = 'usr-system';

        
    }

    /**
     * Registers wp hooks
     *
     * Registers all hooks that should register on start of
     * a plugin.
     * <ul>
     * <li>create post type</li>
     * <li>register css & js</li>
     * <li>register archive template</li>
     * <li>register ajax calls</li>
     * </ul>
     *
     * @return null
     * @since  1.0
     */
    public function addHooks()
    {
        add_action('init', array($this, 'createPostTypes'));

        add_action('wp_enqueue_scripts', array($this, 'frontEnd'));

        add_filter('archive_template', array($this, 'archiveepage'));

        //ajax requests
        $this->registerAjaxRequests(
            array(
            'getUserPosts',
            'getUserTodos',
            'getUserAlbums',
            'getPostComments',
            'getAlbumPhotos'
            )
        );
    }
    /**
     * Registers ajax calls
     *
     * Registers ajax calls for custom requests for logged in and non-logged in users
     *
     * @param array $requests array passed from addHooks function
     *
     * @return null
     * @since  1.0
     */
    public function registerAjaxRequests($requests)
    {

        foreach ($requests as $request) {

            add_action("wp_ajax_nopriv_{$request}", array($this, "getAjaxResults"));
            add_action("wp_ajax_{$request}", array($this, "getAjaxResults"));
        }
    }

    /**
     * Get ajax result
     *
     * <ul>
     * <li>Assigns api for each ajax calls</li>
     * <li>Passes arguments for CURL requests
     * </ul>
     *
     * @return array json_decoded result for curl requests
     *
     * @since 1.0
     */
    public function getAjaxResults()
    {
        $api = $this->_assignApi($_POST);
        echo USR_Helper::handleRequest($api['request'], $api['url'], true);exit;

    }
    /**
     * Assigns respective api
     *
     * Assigns api url and request handle
     *
     * @param array $arguments input from ajax requests
     *
     * @return array consist of api url and request handle
     *
     * @since 1.0
     */
    private function _assignApi($arguments)
    {

        return USR_Helper::apiAdapter(
            $arguments['action'], 
            ($arguments['data'] ? $arguments['data']: '')
        );

    }

    /**
     * Registers js and css
     *
     * Registers plugin specific js and css files
     *
     * @return null
     * @since  1.0 
     */
    public function frontEnd()
    {
        //add foundation
        wp_enqueue_style(
            'foundation-zurb-css', 
            plugin_dir_url(__FILE__).'/css/app.css', 
            array(), 
            strtotime(date('dd-mm-yy H:i:s')), 
            'all'
        );

        wp_enqueue_style(
            'front-userapi-css', 
            plugin_dir_url(__FILE__).'/css/style.css', 
            array(), 
            strtotime(date('dd-mm-yy H:i:s')), 
            'all'
        );


        wp_enqueue_script(
            'userapi-jquery',
            'https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js',
            array(), 
            null, 
            true 
        );

        wp_enqueue_script(
            'front-userapi-js',
            plugin_dir_url(__FILE__).'/js/script.js',
            array('userapi-jquery'), 
            null, 
            true 
        );


        $frontend = array( 
            'main_site' => site_url(), 
            'plugin_path'=>plugin_dir_url(__FILE__),
            'ajax_url' => admin_url('admin-ajax.php')
        );
           wp_localize_script('front-userapi-js', 'frontend', $frontend);

    }
    
    /** 
     * Loads archive template
     *
     * Loads plugin specific archive template
     * fetches default getUsers request on page load
     *
     * @return null
     * @since  1.0 
     */
    public function archiveepage()
    {
        
        try {
            $api = USR_Helper::apiAdapter('getUsers');
            $result= USR_Helper::handleRequest($api['request'], $api['url'], false);
            $result = json_decode($result);
            include_once plugin_dir_path(__FILE__) . 'archive-userapi.php';
            exit;
        }
        catch(Exception $e) {
            echo 'Message: ' .$e->getMessage();
        }
        
    }

    /**
     * Registers custom post type
     *
     * Registers array of custom post type
     *
     * @return object registered post type
     *
     * @since 1.0
     */
    public function createPostTypes()
    {
        foreach ($this->_postTypes as $key => $menu) {

            // $capmenu = ucfirst($menu);
            $capmenu = str_replace('_', ' ', $menu);
        
            register_post_type(
                $menu,
                array(

                'labels' => array(

                'name' => __($capmenu, $menu),

                'singular_name' => __($capmenu, $menu),

                'add_new_item' => __("Add New $capmenu", $menu),

                'edit_item' => __("Edit $capmenu", $menu),

                'view_item' => __("All $capmenu", $menu),

                ),

                'hierarchical' => false,

                    'description' => "$capmenu full width grid",

                    'supports' => array( 'editor', 'title' ),

                    'public' => true,

                    'show_ui' => true,

                    'show_in_menu' => $this->_parentMenuSlug,

                    'menu_position' => 5,

                    'menu_icon' => 'dashicons-grid-view',

                    'show_in_nav_menus' => false,

                    'publicly_queryable' => true,

                    'exclude_from_search' => true,

                    'has_archive' => true,

                    'query_var' => true,

                    'can_export' => true,

                'rewrite' => array(

                'slug' => $capmenu,

                'with_front' => true,

                'feeds' => true,

                'pages' => true

                ),

                'capability_type' => 'post'

                )
            );

        }
    }
}

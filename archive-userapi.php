<?php 
/**
 * Archive template
 *
 * Archive template file for plugin
 *
 * php version 7.2.1
 *
 * @category Archive_File
 * @package  User_Api
 * @author   Kalaivani Nair <kalaivani.dc@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @version  GIT: git@github.com:karen-nair/testcomposer2.git
 * @link     mailto:kalaivani.dc@gmail.com 
 */
get_header();?>

      <div class="grid-x grid-padding-x">
        <div class="large-12 cell">
          <h1>User data</h1>
        </div>
      </div>

       <div class="grid-x grid-padding-x">
        <div class="large-12 cell">
            <table width="100%" id="userTable">
                <th>ID</th>
                <th>NAME</th>
                <th>USERNAME</th>
                <th>EMAIL</th>
                <th>ADDRESS</th>
                <th>PHONE</th>
                <th>WEBSITE</th>
                <th>COMPANY</th>


                <?php

                foreach ($result as $key => $row) {

                    ?>
                    <tr id="<?php echo $row->id;?>">
                        <td><a href="#"><?php echo $row->id;?></a></td>
                        <td><a href="#"><?php echo $row->name;?></a></td>
                        <td><a href="#"><?php echo $row->username;?></a></td>
                        <td><?php echo $row->email;?></td>
                        <td><?php
                        echo $row->address->suite." ,";
                        echo $row->address->street." ,";
                        echo $row->address->city.' ,';
                        echo $row->address->zipcode;
                        echo "<br/>";
                        echo '<i>lat= '.$row->address->geo->lat. '</i>';
                        echo '<i>lng= '.$row->address->geo->lng.'</i>';
                    
                        ?></td>
                        <td><?php echo $row->phone;?></td>
                        <td><?php echo $row->website;?></td>
                        <td><?php
                        echo '<b>'.$row->company->name.'</b><br/>';
                        echo '<i>'.$row->company->catchPhrase.'</i><br/>';
                        $tags = explode(' ', $row->company->bs);
                        foreach ($tags as $tag) {
                            ?>
                            <span class="tag"><?php echo $tag;?></span>
                            <?php
                        }
                        ?></td>

                        
                    </tr>
                    <?php
                }
                ?>
            </table>


        </div>
    </div>

  <?php get_footer(); ?>


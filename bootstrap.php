<?php
/**
 * Plugin Bootstrap
 *
 * Userapi plugin bootsrap file
 *
 * php version 7.2.1
 *
 * @category Userapi_Bootstrap_File
 * @package  User_Api
 * @author   Kalaivani Nair <kalaivani.dc@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @version  GIT: git@github.com:karen-nair/testcomposer2.git
 * @link     mailto:kalaivani.dc@gmail.com 
 */

/**
 * Autoload classes
 *
 * Autoload classes inside userapi plugin 
 * Each class is encapsulated with namespace
 * Triggered by plugin main file
 *
 * @param string $className class loaded
 *
 * @return string namespaced file path
 */
function autoloadFiles($className)
{

    if (false === strpos($className, PLUGIN_KEYWORD)) {
        return;
    }

    $file_parts = explode('\\', $className);
    $namespace = '';

    for ($i = count($file_parts) - 1; $i > 0; $i--) {

        $current = strtolower($file_parts[$i]);
        $current = str_ireplace('_', '-', $current);
        $current = str_ireplace(PLUGIN_KEYWORD.'-', '', $current);

        if (count($file_parts) - 1 === $i) {

            if (strpos(
                strtolower(
                    $file_parts[count($file_parts) - 1]
                ), 'interface'
            )
            ) {
                echo "interface";
            } else {
                $file_name = "class-$current.php";
                
            }

        } else {
            $namespace = '/'.$current.$namespace;
        }
    }

    $file_path = trailingslashit(dirname(__FILE__).$namespace);
    $file_path .= $file_name;

    include_once $file_path;
}

<?php
/**
 * The following snippets uses `PLUGIN` to prefix
 * the constants and class names. You should replace
 * it with something that matches your plugin name.
 */
// define test environment
define( 'USERAPI_PHPUNIT', true );

// define fake ABSPATH
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', sys_get_temp_dir() );
}
// define fake PLUGIN_ABSPATH
if ( ! defined( 'USERAPI_ABSPATH' ) ) {
	define( 'USERAPI_ABSPATH', sys_get_temp_dir() . '/wp-content/plugins/userapi/' );
}

require __DIR__.'/../../class-core.php';
require __DIR__.'/../../class-helper.php';
require_once __DIR__ . '/../../../../../vendor/autoload.php';

// Include the class for UserapiTestCase
require_once __DIR__ . '/inc/UserapiTestCase.php';
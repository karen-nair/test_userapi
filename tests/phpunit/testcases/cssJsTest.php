<?php
use \Brain\Monkey\Functions;

class USR_CoreTest extends \UserapiTestCase {

	private $ajax = array(
        	'getUserPosts',
        	'getUserTodos',
        	'getUserAlbums',
        	'getPostComments',
        	'getAlbumPhotos'

        );

	public function testRegisterAjaxRequests() {


		$class = new \USR\USR_Core();
        $class->registerAjaxRequests($this->ajax);

        foreach ($this->ajax as $key => $value) {
        	self::assertTrue( has_action("wp_ajax_nopriv_{$value}", "\USR\USR_Core->getAjaxResults()") );
        }
    }

    public function test_AssignApi() {

    	$param = 'userID';

    	foreach ($this->ajax as $key => $value) {

    		if ($value == 'getPostComments') {
    			$param = 'postID';
    		} else if ($value == 'getAlbumPhotos') {
    			$param = 'albumID';
    		}
    		self::assertArrayHasKey('url', \USR\USR_Helper::apiAdapter("{$value}", array("{$param}"=>1)));
    		self::assertArrayHasKey('request', \USR\USR_Helper::apiAdapter("{$value}", array("{$param}"=>1)));
    	}
    	

    }

}